## Lost Access details

<!-- NOTE:
Before starting, please read carefully the following requirements.

In order to gain access back to your account you'll have to list all information that you know about your account.
This is in order to prove that you are the owner of the account.

Any data (except for passwords) that only you as the account owner can hold, is useful for this.

-->

### Master Account Information
<!-- insert after this line the numeric ID (MID) or email address associated to your Master Account -->

<!-- Please name us your new email that we should change your current one too -->

MID:
Current/Old Email:
New Email:

NPC:AutoCommands settings:
- ...
- ...

Game Account names:
- ...
- ...

#### Noteworthy items that you own

<!--

Are there some very specific items that not many own? List them here.

-->

- ...
- ...

#### Screenshots of characters

<!--
all screenshots that you own of your account go here. We'd expect you to have at least 5.

But we won't expect you to send us more than 50.

A good idea is to send us screenshots of characters in their early stages (low level) and late stages (high level)

-->


### Notes
<!-- insert after this line any additional notes you wish to add -->

<!-- please ignore the lines below -->
/confidential
