## Request Details

<!-- NOTE:

By admitting buying ingame things or ingame services in exchange for money or things OUTSIDE of the game and coming to us first, you will receive lower punishment:
- All your items will be deleted.
- All your zeny will be deleted.
- But you keep your characters.

-->

### Master Accounts Information
<!-- insert after this line the numeric IDs or email address of your master account -->

### Trades

- Date / Year:
- Characters involved:
- Zeny / Items / Service received:

### Notes
<!-- insert after this line any additional notes you wish to add -->


<!-- please ignore the lines below -->
/confidential
