## Report Information

<!-- Any report needs a server time stamp, obtainable via `@time` in exactly that format. Failure to provide one will make the report invalid. -->

<!-- Any report with a replay or a video file needs timestamps of when the thing you're accusing the reportee of is happening. Failure to provide them will make the report invalid. -->

<!-- All mob related reports need uncropped screenshots or a replay file. If you only provide a recorded video, your report will be invalid. -->

<!-- All reports in which you: threaten a person with a report OR made fun of them being banned after a report, are invalid. -->
<!-- We strongly suggest to not mention in any shape or form, that you will report someone or that he might be banned. -->
<!-- If you fail to comply, your report will be marked invalid. -->

### Reportee
<!-- insert after this line the name of the player you are reporting -->

### Report Reason
<!-- insert after this line the reason for the report and the number of the infringed rule as in our ToS -->

### Report Details
<!-- insert after this line the content of your report, including the date and server time of the reported incident -->
<!-- include any details that might be relevant to the case. -->

### Attachments
<!-- insert after this line any evidence (screenshots, videos, replays, links) that might be relevant to the case -->

<!-- NOTE: tickets are confidential. We will not show the evidence to anyone else and we will not disclose the punishment of the reportee  -->
