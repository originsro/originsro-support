## GRF Approval Request

<!-- NOTE:
Before starting, please read carefully the following requirements and conditions.

GRF mods are manually reviewed by our staff and may require a long time to be approved, depending on their complexity.

Before submitting a GRF mod for approval, please get familiar with the ToS Topic VI.1. Client Modifications and Third Party Software:

https://originsro.org/tos#s6t1-modifications

Approved GRF mods are digitally signed to be used by a specific Master Account. If two people wish to use the same GRF mod, they need to submit separate approval requests.
Subsequent requests to approve an identical GRF for different Master Accounts don't require a full manual validation of the file contents, and are processed more quickly.

The use of a custom.grf file on a shared computer is not supported.

GRF mod approval requests are not to be considered tacitly accepted. Please refrain from using your custom.grf until explicitly accepted.
-->

### Master Account Information
<!-- insert after this line the numeric ID or email address associated to your Master Account -->

### GRF Description
<!-- insert after this line a brief description of your GRF modification -->

### Modified Files
<!--
Insert here a bullet list of every file included in your custom.grf, with the reason for its modification (i.e. what the modification tries to achieve or why).
Each line should have the following format:

- `data/foo.xyz`: Information about foo.xyz
-->

### GRF File
<!-- Insert after this line a link to the custom.grf file you want to submit (NOTE: the file name MUST be custom.grf, and it must only contain the files listed above. A full, modified, data.grf will NOT be accepted.) -->

### Notes
<!-- insert after this line any additional notes you wish to add -->

<!-- please ignore the lines below -->
/confidential
