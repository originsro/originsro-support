### Guild Leader Change

<!-- A guild leader change can only be done via guest-access if the new guild leader is on the same master account as the current one. -->
<!-- In any other situation the guild leader will need to authorize the guild leader change via a mail to support@originsro.org with a copy of this issue -->
<!-- If you are the guild leader, you're good to go. -->

#### Guild Name
<!-- Insert after this line the name of the Guild of which you wish to change the leader -->

#### Current Guild Leader
<!-- Insert after this line the name of the current Guild Leader -->

#### New Guild Leader
<!-- Insert after this line the name of the new Guild Leader -->