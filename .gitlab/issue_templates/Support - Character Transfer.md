## Transfer Details

<!-- NOTE:
Before starting, please read carefully the following requirements and conditions.

Character transfers are free of charge, but may require some time to be completed.
Please be patient if you don't receive a response within a few days, as the requests are usually handled in weekly batches.

In order to minimize the wait time and the chances that your request is rejected, please make sure all the following conditions are met.

- The source and destination Game Accounts must be part of the same Master Account.
- The source and destination Game Accounts must be the same gender.
- The destination Game Account must exist and be completely empty.
- Every Character in the source Game Account must be completely offline (and not at the character selection screen) at the time of the transfer.
- The transferred Character must not be in any Guild or Party at the time of the transfer.
- The transferred Character must not have any Pet or Homunculus at the time of the transfer (if present, they should be put back into their Pet Egg or Vaporized, respectively).
-->

### Master Account Information
<!-- insert after this line the numeric ID or email address associated to your Master Account -->

<!-- ---- 8< ----------------------------------------------------------------------------------------------------- -->
### Character Transfer

#### Character Name
<!-- Insert after this line the name of the character you wish to transfer -->

#### Source Account
<!-- Insert after this line the name of the source Game Account -->

#### Destination Account
<!-- Insert after this line the name of the destination Game Account -->

<!-- ---- 8< ----------------------------------------------------------------------------------------------------- -->
<!-- (if you want to transfer multiple characters, copy and paste the "Character Transfer" section above as many times as you need) -->

### Notes
<!-- insert after this line any additional notes you wish to add -->

<!-- please ignore the lines below -->
/confidential
