## Request Details

<!-- NOTE:
Before starting, please read carefully the following requirements and conditions.

Your IP only needs to be whitelisted if multiple people in the same household intend to play WoE at the same time.
For non-WoE use, it is not necessary to submit any whitelist request.

Please be patient if you don't receive a response within a few days, as the requests are usually handled in weekly batches, before the WoE.

Before a whitelist request is accepted, your login history will be verified for compliance with ToS Topic IV.9. Master Accounts:
https://originsro.org/tos#s4t9-master-accounts

Your request will be rejected if any of the following is detected in your recent login history:

- Concurrent access to multiple Master Accounts by the same Player, without using the Guest Access system
- Master Account sharing between multiple Players, without using the Guest Access system
- Onwership of multiple Master Accounts by the same Player

If your login history may contain any such occurrence, please mention it in the Notes section, in order to rectify your situation.

Further verifications of your login history may be performed during the entirety of your requested whitelist duration, to prevent abuse of this feature.
-->

### Request Reason
<!-- insert after this line the reason for this request (i.e. why there will be multiple people playing WoE from your IP address) -->

### Master Accounts Information
<!-- insert after this line the numeric IDs or email addresses associated to each Master Account you wish to whitelist -->

### Whitelist Expiration
<!-- please specify whether the whitelist request is to be considered permanent (until explicitly cancelled by you), or it should expire after a specified date -->

### Notes
<!-- insert after this line any additional notes you wish to add -->

<!-- please ignore the lines below -->
/confidential
