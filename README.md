# Welcome to the Arcadia Online Support Help Desk

![Arcadia Online](.resources/topimage.jpg)

## Arcadia Online Main Website and Downloads

If you're just looking for Arcadia Online or want to download the client, please head to https://arcadia-online.org/

## Support and Player Reports

If you want to open a **support ticket** or **report a player**, this is the right place!

You can [create a **new ticket**](https://gitlab.com/arcadia-online/arcadia-support/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

Please note that in order to post new ticket, you'll need to [sign in or sign up for a free GitLab account](https://gitlab.com/users/sign_in?redirect_to_referer=yes).

## Technical Support

If you're looking for **technical support** [Forum](https://bbs.arcadia-online.org/) or [Discord Server](https://arcadia-online.org/discord).

## Bug and Feature Tracker

If you're looking for **bug reports** or **feature suggestions** concerning Arcadia Online, please head to our [Bug and Feature Tracker](https://gitlab.com/arcadia-online/arcadia-online).

## Arcadia Online useful links

- Main website: https://arcadia-online.org
- Forum: https://bbs.arcadia-online.org
- Control Panel: https://cp.arcadia-online.org
- Wiki: https://wiki.arcadia-online.org
- Bug and Feature Tracker: https://gitlab.com/arcadia-online/arcadia-online
- Help Desk: https://gitlab.com/arcadia-online/arcadia-support
